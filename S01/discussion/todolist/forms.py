from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label="Username", max_length=200)
	password = forms.CharField(label="Password", max_length=200)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class AddEventForm(forms.Form):
	event_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label="Status", max_length=50)

class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label="Status", max_length=50)

class UpdateProfileForm(forms.Form):
	first_name = forms.CharField(label="First Name", max_length=100)
	last_name = forms.CharField(label="Last Name", max_length=100)
	password = forms.CharField(label="Password", required=False)
class RegisterForm(forms.Form):
	user_name = forms.CharField(label="Username", max_length=50)
	user_fname = forms.CharField(label="First Name", max_length=50)
	user_lname = forms.CharField(label="Last Name", max_length=100)
	email = forms.EmailField(label="Email", max_length=100)
	password = forms.CharField(label="Password", max_length=50)
	con_password = forms.CharField(label="Confirm Password", max_length=50)
