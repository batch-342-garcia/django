from django.contrib import admin

from .models import ToDoItem, EventList

class EventListAdmin(admin.ModelAdmin):
    list_display = ('event_name', 'description', 'status', 'event_date')

admin.site.register(ToDoItem)

admin.site.register(EventList, EventListAdmin)

# admin.site.register()