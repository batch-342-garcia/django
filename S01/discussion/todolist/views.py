from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password

# from django.template import loader

# Local import
from .models import ToDoItem, EventList
from .forms import LoginForm, AddTaskForm, AddEventForm, UpdateTaskForm, UpdateEventForm, RegisterForm, UpdateProfileForm

# Create your views here.
def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	eventitem_list = EventList.objects.filter(user_is_id=request.user.id)
	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# template = loader.get_template("todolist/index.html")
	# context = {'todoitem_list' : todoitem_list}
	# return HttpResponse(template.render(context, request))

	# simplier way
	context = { 
	'eventitem_list' : eventitem_list,
	'todoitem_list' : todoitem_list,
	'user' : request.user
	}
	return render(request, "todolist/index.html", context)
# for todoitem
def todoitem(request, todoitem_id):
	# response = "You are viewing the details of %s"
	# return HttpResponse(response % todoitem_id)
	# The model_to_dict() allows to convert models into dictionaries
	# get() allows to retrive a record using it primary key(pk)
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

# this is for eventlist
def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(EventList, pk=eventitem_id)
	return render(request, "todolist/eventitem.html", model_to_dict(eventitem))

def register(request):
	context = {}

	if request.method == 'POST':
		form = RegisterForm(request.POST)

		if form.is_valid():
			user_name = form.cleaned_data['user_name']
			user_fname = form.cleaned_data['user_fname']
			user_lname = form.cleaned_data['user_lname']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']


			con_password = form.cleaned_data['con_password']

			if password == con_password:
				
				# con_password = form.cleaned_data['con_password']
				duplicates = User.objects.filter(email=email)

				if not duplicates:
					# print(hashed_password)
					
					hashed_password = make_password(password)
					new_user = User(
						username = user_name,
						first_name = user_fname,
						last_name = user_lname,
						email = email,
						password = hashed_password
					)
					new_user.save()
					return redirect("todolist:login")
				else:
					context = {
						"error": True
					}
			else:
				context = {
					"password_mismatched": True
				}
		else:
			context = {
					"is_form_invalid": True
				}
				
	return render(request, "todolist/register.html", context)


def update_profile(request, user_id):
		
	user = User.objects.filter(pk=user_id)

	if not user:

		context = {
			"error" : True
		}
	else:
		if request.method == 'POST':
			form = UpdateProfileForm(request.POST)

			if form.is_valid() == False:
				form = UpdateProfileForm()

			else:
				first_name = form.cleaned_data['first_name']
				last_name = form.cleaned_data['last_name']
				password = form.cleaned_data['password']

				user[0].first_name = first_name
				user[0].last_name = last_name

				if password:
					user[0].set_password(password)
					user[0].save()
					update_session_auth_hash(request, user[0])
					return redirect("todolist:login")

				else:
					user[0].save()
					
				return redirect("todolist:index")
				
			
		context = {
			"user" : request.user,
			"user_id" : user_id,
			"first_name" : user[0].first_name,
			"last_name" : user[0].last_name
		}
		
	return render(request, "todolist/update_profile.html", context)

def login_view(request):
	context = {}

	if request.method == 'POST':
		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(request, username=username, password=password)

			print(user)

			if user is not None: 
			# Save creates a record in the django_session table in the database
				print(user.password)
				login(request, user)
				# redirect the user to the index.html page
				return redirect("todolist:index")
			else:
				print("user is correct password is mismatched")
				context = {
					"error" : True
					}
	
	return render(request, "todolist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("todolist:login")

def add_task(request):

	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			# Checks if a task already exists in the database
			user = request.user
			duplicates = ToDoItem.objects.filter(user=user, task_name=task_name)

			if not duplicates:

				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")
			
			else:

				context = {
					"error": True
				}

	return render(request, "todolist/add_task.html", context)

def add_event(request):

	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			user = request.user
			duplicates = EventList.objects.filter(user_is=user, event_name=event_name)

			if not duplicates:
				EventList.objects.create(event_name=event_name, description=description, event_date=timezone.now(), user_is_id=request.user.id)
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}

	return render(request, "todolist/add_event.html", context)

def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user" : request.user,
		"todoitem_id" : todoitem_id,
		"task_name" : todoitem[0].task_name,
		"description" : todoitem[0].task_name,
		"status" : todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

		if todoitem:
			todoitem[0].task_name = task_name
			todoitem[0].description = description
			todoitem[0].status = status
			todoitem[0].save()
			return redirect("todolist:index")

		else:

			context = {
				"error" : True
			}

	return render(request, 'todolist/update_task.html', context)

def update_event(request, eventitem_id):

	eventitem = EventList.objects.filter(pk=eventitem_id)

	context = {
		"user" : request.user,
		"eventitem_id" : eventitem_id,
		"event_name" : eventitem[0].event_name,
		"description" : eventitem[0].description,
		"status" : eventitem[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']
		
		if eventitem:
			eventitem[0].event_name = event_name
			eventitem[0].description = description
			eventitem[0].status = status
			eventitem[0].save()
			return redirect("todolist:index")
		else:
			context = {
				"error" : True
			}

	return render(request, 'todolist/update_event.html', context)

def delete_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()

	return redirect("todolist:index")

def delete_event(request, eventitem_id):

	eventitem = EventList.objects.filter(pk=eventitem_id).delete()

	return redirect("todolist:index")